
-- Use the `ref` function to select from other models

select *
from {{ ref('monthly_amount_agg') }}
where year(month) = 2022
