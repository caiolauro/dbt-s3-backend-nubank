
/*
    Welcome to your first dbt model!
    Did you know that you can also configure models directly within SQL files?
    This will override configurations stated in dbt_project.yml

    Try changing "table" to "view" below
*/

{{ config(materialized='table') }}

select 
  date_trunc('month', time)::date as month
, sum(case when is_income then amount else null end) as total_income
, sum(case when is_income=false then amount else null end) as total_spend
, total_income - total_spend as savings
, sum(case when is_income then amount else null end)/30 as average_income
, sum(case when is_income=false then amount else null end)/30 as average_spend

from nubank_api.clauro.transactions_details
group by 1
order by 1

/*
    Uncomment the line below to remove records with null `id` values
*/

-- where id is not null
