
-- Use the `ref` function to select from other models

select *
from {{ ref('daily_amount_agg') }}
where date_trunc(month,day_date) >= dateadd('month', -6, date_trunc(month,day_date))

